package ar.com.wolox.androidtechnicalinterview.data

import ar.com.wolox.androidtechnicalinterview.BuildConfig
import ar.com.wolox.androidtechnicalinterview.data.network.RestApi
import ar.com.wolox.androidtechnicalinterview.data.network.endPoint.GifEndPoint
import ar.com.wolox.androidtechnicalinterview.models.GifResponse
import io.reactivex.Observable

class ApiDataManager {
    private var gifEndPoint: GifEndPoint

    constructor(restApi: RestApi<GifEndPoint>) {
        gifEndPoint = restApi.getClient(GifEndPoint::class.java)!!
    }

    fun requestGifBySearchQuery(s: String, l: String): Observable<GifResponse> {
        return gifEndPoint.search(
            search_query = s,
            api_key = BuildConfig.API_KEY,
            lang = l
        )
    }

    fun requestGifTrending(l: String): Observable<GifResponse> {
        return gifEndPoint.trending(api_key = BuildConfig.API_KEY, lang = l)
    }
}