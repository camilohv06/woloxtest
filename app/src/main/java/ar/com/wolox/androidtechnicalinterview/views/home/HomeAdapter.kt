package ar.com.wolox.androidtechnicalinterview.views.home

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ProgressBar
import ar.com.wolox.androidtechnicalinterview.R
import ar.com.wolox.androidtechnicalinterview.models.Gif
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.item_gif.view.*

class HomeAdapter(
    private val mContext: Context,
    private val items: List<Gif>,
    private val listener: (Gif) -> Unit,
    private val longListener: (Gif, Boolean) -> Unit
) :
    BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = convertView ?: LayoutInflater.from(mContext).inflate(R.layout.item_gif, parent, false) as CardView
        val holder = v.tag as? ProductViewHolder ?: ProductViewHolder(v)
        v.tag = holder

        onBindViewHolder(holder, position)
        return v
    }

    override fun getItem(position: Int): Any? = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.size

    private fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val item = items[position]
        holder.name.text = item.title
        loadGif(item.images.fixed_height_downsampled.url, holder.image, holder.progressBar)
        holder.cardView.setOnClickListener { listener(item) }
        holder.cardView.setOnLongClickListener {
            holder.buttonFavorite.switchState()
            longListener(item, holder.buttonFavorite.isIconEnabled)
            true
        }
        holder.buttonFavorite.setIconEnabled(item.enable, true)
    }

    private fun loadGif(url: String, imageView: ImageView, progressBar: ProgressBar) {
        Glide.with(mContext)
            .load(url)
            .listener(requestListener(progressBar))
            .error(R.drawable.ic_failed)
            .skipMemoryCache(true)
            .into(imageView)
    }

    private fun requestListener(progressBar: ProgressBar): RequestListener<Drawable> {
        return object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.visibility = View.GONE //TODO put failed image
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.visibility = View.GONE
                return false
            }
        }
    }

    class ProductViewHolder(v: View) {
        val name = v.txtNameGif!!
        val image = v.imageGif!!
        val progressBar = v.progressGif!!
        val cardView = v.card_view_gif!!
        val buttonFavorite = v.buttonFavorite!!
    }
}