package ar.com.wolox.androidtechnicalinterview.utils

import com.bumptech.glide.load.resource.gif.GifDrawable

interface DownloadImageCompletedInterface {
    fun onTaskComplete(result: GifDrawable)
    fun onTaskWithError()
}