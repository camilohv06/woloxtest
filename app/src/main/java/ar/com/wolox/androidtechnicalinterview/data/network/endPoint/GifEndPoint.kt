package ar.com.wolox.androidtechnicalinterview.data.network.endPoint

import ar.com.wolox.androidtechnicalinterview.models.GifResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GifEndPoint {
    @GET("search?")
    fun search(
        @Query("q") search_query: String,
        @Query("api_key") api_key: String,
        @Query("lang") lang: String
    ): Observable<GifResponse>

    @GET("trending?")
    fun trending(
        @Query("api_key") api_key: String,
        @Query("lang") lang: String
    ): Observable<GifResponse>
}