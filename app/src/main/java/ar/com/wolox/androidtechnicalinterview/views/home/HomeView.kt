package ar.com.wolox.androidtechnicalinterview.views.home

import android.view.MenuItem
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.Gif

interface HomeView {
    fun showProgress()
    fun hideProgress()
    fun setItems(items: List<Gif>)
    fun requestPermissionsWhenStart()
    fun updateGifList(items: List<Gif>)
    fun saveFavoriteGif(favorite: Favorite)
    fun deleteFavoriteGif(id: String)
    fun setTitleMenu(item: MenuItem?, id: Int)
    fun downloadImageToShare(url: String)
}
