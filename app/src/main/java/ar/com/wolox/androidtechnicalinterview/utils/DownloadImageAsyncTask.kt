package ar.com.wolox.androidtechnicalinterview.utils

import android.content.Context
import android.os.AsyncTask
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.gif.GifDrawable
import java.lang.ref.WeakReference

class DownloadImageAsyncTask(context: Context) : AsyncTask<String, Void, GifDrawable>() {
    private var mContext: WeakReference<Context> = WeakReference(context)
    private var mCallback: DownloadImageCompletedInterface = mContext.get() as DownloadImageCompletedInterface

    override fun doInBackground(vararg values: String): GifDrawable? {
        val context = mContext.get()
        return if (context != null) {
            Glide.with(context).asGif().load(values[0]).submit().get()
        } else {
            null
        }
    }

    override fun onPostExecute(results: GifDrawable?) {
        if (results != null) {
            mCallback.onTaskComplete(results)
        } else {
            mCallback.onTaskWithError()
        }
    }
}