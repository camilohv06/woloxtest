package ar.com.wolox.androidtechnicalinterview.utils

import android.graphics.Bitmap
import android.os.Environment
import ar.com.wolox.androidtechnicalinterview.MainApplication
import com.bumptech.glide.load.resource.gif.GifDrawable
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.nio.ByteBuffer

/**
 * It'method allow save the bitmap in the device, receive a gifDrawable to build the bitmap
 */
@Throws(IOException::class)
private fun createBitmapFile(bitmap: Bitmap): File {
    val directoryName = getDirectoryName("abc", ".jpg")

    try {
        // Compress the bitmap and save in jpg format
        val stream: OutputStream = FileOutputStream(directoryName)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        stream.flush()
        stream.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return directoryName
}

/**
 * It'method allow save the gif in the device, receive a gifDrawable to build the gif
 */
@Throws(IOException::class)
fun createGifFile(gifDrawable: GifDrawable): File? {
    val directoryName = getDirectoryName("abc", ".gif")

    try {
        val byteBuffer = gifDrawable.buffer
        val output = FileOutputStream(directoryName)
        val bytes = ByteArray(byteBuffer.capacity())
        (byteBuffer.duplicate().clear() as ByteBuffer).get(bytes)
        output.write(bytes, 0, bytes.size)
        output.close()
        return directoryName

    } catch (e: IOException) {
        println(e.localizedMessage)
    }
    return null
}

/**
 * Generate the File where should be the created image
 */
fun getDirectoryName(id: String, format: String): File {
    val storageDir = MainApplication.applicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(getFileName(id), format, storageDir)
}

/**
 * Generate the image name
 */
fun getFileName(id: String): String {
    return "GIF_$id"
}