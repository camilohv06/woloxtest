package ar.com.wolox.androidtechnicalinterview.data.network

import ar.com.wolox.androidtechnicalinterview.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestApi<T> {

    private var retrofit: Retrofit? = null

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.URL_PATH)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getHttpClient())
            .build()
    }

    fun getClient(endPoint: Class<T>): T? {
        return retrofit?.create<T>(endPoint)
    }

    private fun getHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) logging.level = HttpLoggingInterceptor.Level.BODY else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
            .readTimeout(BuildConfig.TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(BuildConfig.TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .build()
    }
}