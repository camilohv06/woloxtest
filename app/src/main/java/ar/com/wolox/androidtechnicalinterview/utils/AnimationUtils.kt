package ar.com.wolox.androidtechnicalinterview.utils

import android.view.View
import android.view.animation.AnimationUtils
import ar.com.wolox.androidtechnicalinterview.MainApplication

/**
 * It'method animate a view, receive the view and the id of the animation
 */
fun animateView(view: View?, animation: Int) {
    if (view != null) {
        val loadAnimation = AnimationUtils.loadAnimation(MainApplication.applicationContext(), animation)
        view?.startAnimation(loadAnimation)
    }
}