package ar.com.wolox.androidtechnicalinterview.utils

import android.widget.Toast
import ar.com.wolox.androidtechnicalinterview.MainApplication

fun showMessage(message: String) {
    Toast.makeText(MainApplication.applicationContext(), message, Toast.LENGTH_LONG).show()
}