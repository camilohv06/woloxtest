package ar.com.wolox.androidtechnicalinterview.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import ar.com.wolox.androidtechnicalinterview.BuildConfig
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.FavoriteDao
import ar.com.wolox.androidtechnicalinterview.utils.Constants

@Database(entities = [Favorite::class], version = BuildConfig.VERSION_DB)
abstract class FavoriteDatabase : RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDao

    companion object {
        var TEST_MODE = false
        private const val DATABASE_NAME = Constants.DATABASE_NAME
        @Volatile
        private var INSTANCE: FavoriteDatabase? = null

        fun getInstance(context: Context): FavoriteDatabase? {
            if (TEST_MODE) {
                INSTANCE ?: synchronized(this) {
                    INSTANCE = Room.inMemoryDatabaseBuilder(context.applicationContext, FavoriteDatabase::class.java)
                        .allowMainThreadQueries().build()
                }
            } else {
                INSTANCE ?: synchronized(this) {
                    INSTANCE =
                        Room.databaseBuilder(context.applicationContext, FavoriteDatabase::class.java, DATABASE_NAME)
                            .build()
                }
            }
            return INSTANCE
        }

        private fun close() {
            INSTANCE?.close()
        }
    }
}