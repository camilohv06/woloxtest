package ar.com.wolox.androidtechnicalinterview.views.home

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import ar.com.wolox.androidtechnicalinterview.R
import ar.com.wolox.androidtechnicalinterview.database.FavoritesViewModel
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.Gif
import ar.com.wolox.androidtechnicalinterview.utils.*
import com.bumptech.glide.load.resource.gif.GifDrawable
import kotlinx.android.synthetic.main.activity_home.*
import java.io.File


/**
 * MIT License
 *
 * Copyright (c) 2019 Wolox S.A
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
class HomeActivity : AppCompatActivity(), HomeView, DownloadImageCompletedInterface {

    private val presenter = HomePresenter(this)
    private var menuStatus = true
    private var gifList: List<Gif>? = null
    private var gifListFavorites: List<Gif>? = null
    private lateinit var favoritesViewModel: FavoritesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fetchRemoteConfig()
        setContentView(R.layout.activity_home)
        setUI()
        setListenerSearchView()
        initViewModelDB()
        requestPermissionsWhenStart()
        addObserver()

        presenter.loadGifTrending(resources.getString(R.string.app_lang))

    }

    private fun fetchRemoteConfig() {
        presenter.fetchRemoteConfig(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
        gifList = null
        gifListFavorites = null
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId
        if (id == R.id.menu_favorite) {
            changeTitleMenu(menuStatus, item)
            loadItems(menuStatus)
            menuStatus = !menuStatus
            presenter.changeVisibilitySearchView(searchViewGif, menuStatus)

        }
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        progressHomeScreen.visibility = View.VISIBLE
        gridGif.visibility = View.GONE
    }

    override fun hideProgress() {
        progressHomeScreen.visibility = View.GONE
        gridGif.visibility = View.VISIBLE
    }

    override fun setTitleMenu(item: MenuItem?, id: Int) {
        if (item != null) item.title = resources.getString(id)
    }

    override fun requestPermissionsWhenStart() {
        if (!presenter.hasStoragePermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
    }

    override fun setItems(items: List<Gif>) {
        var newItems = items
        if (gifListFavorites != null) {
            newItems = presenter.setFavoriteStatus(items, gifListFavorites!!)
        }
        gridGif.adapter = HomeAdapter(this, newItems, presenter::onItemClicked, presenter::onItemLongClicked)
        animateView(gridGif, R.anim.slide_up)
    }

    override fun updateGifList(items: List<Gif>) {
        gifList = items
    }

    override fun saveFavoriteGif(favorite: Favorite) {
        favoritesViewModel.saveFavoriteGif(favorite)
    }

    override fun deleteFavoriteGif(id: String) {
        if (id != "") favoritesViewModel.deleteFavoriteGif(id)
    }

    override fun downloadImageToShare(url: String) {
        searchViewGif.clearFocus()
        DownloadImageAsyncTask(this).execute(url)
    }

    override fun onTaskComplete(result: GifDrawable) {
        hideProgress()
        val file = getImageFile(result)
        if (file != null) {
            presenter.shareWitSocialNetworks(this, file)
        } else {
            showMessage(resources.getString(R.string.txt_error_no_share_gif))
        }
    }

    override fun onTaskWithError() {
        hideProgress()
        showMessage(resources.getString(R.string.txt_error_no_download_gif))
    }

    private fun setUI() {
        presenter.setUIGridGif(gridGif)
    }

    private fun setListenerSearchView() {
        searchViewGif.setOnQueryTextListener(onQueryListener())
    }

    private fun onQueryListener(): SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter.validateQuery(query, resources.getString(R.string.app_lang))
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.isEmpty()) {
                    presenter.getGifTrending(resources.getString(R.string.app_lang))
                }
                return true
            }
        }
    }

    private fun initViewModelDB() {
        favoritesViewModel = run {
            ViewModelProviders.of(this).get(FavoritesViewModel::class.java)
        }
    }

    private fun addObserver() {
        val observer = Observer<List<Favorite>> { favorites ->
            if (favorites != null) {
                gifListFavorites = presenter.convertToGifDTO(favorites)
            }
        }
        favoritesViewModel.contacts.observe(this, observer)
    }

    private fun changeTitleMenu(status: Boolean, item: MenuItem?) {
        if (item != null) {
            presenter.changeTitleMenu(item, status)
        }
    }

    private fun loadItems(status: Boolean) {
        if (status) gifListFavorites?.let { setItems(it) } else gifList?.let { setItems(it) }
    }

    private fun getImageFile(image: GifDrawable): File? {
        if (!presenter.hasStoragePermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        } else {
            try {
                return createGifFile(image)
            } catch (ex: Exception) {
                showMessage(resources.getString(R.string.txt_error_no_save_image))
            }
        }
        return null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 0) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.loadGifTrending(resources.getString(R.string.app_lang))
            }
        }
    }
}
