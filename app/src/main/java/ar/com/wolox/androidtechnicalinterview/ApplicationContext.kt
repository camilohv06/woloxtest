package ar.com.wolox.androidtechnicalinterview

import android.app.Activity
import android.app.Application
import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

class MainApplication : Application() {

    private lateinit var remoteConfig: FirebaseRemoteConfig

    init {
        instance = this
    }

    companion object {
        private var instance: MainApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        fun getRemoteConfig(): FirebaseRemoteConfig {
            return instance!!.remoteConfig
        }

        fun setRemoteConfig(config: FirebaseRemoteConfig) {
            instance!!.remoteConfig = config
        }
    }

    override fun onCreate() {
        super.onCreate()
        remoteConfig = FirebaseRemoteConfig.getInstance()

        val cacheExpiration: Long = if (BuildConfig.DEBUG) {
            0
        } else {
            3600 * 12
        }

        remoteConfig.setConfigSettingsAsync(
            FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(cacheExpiration)
                .build()
        )
        remoteConfig.setDefaults(R.xml.remote_config_defaults)
    }

    private fun fetchRemoteConfig(activity: Activity) {
        // [START fetch_config_with_callback]
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    val updated = task.getResult()
                    Log.d(TAG, "Config params updated: $updated")
                }
            }
        // [END fetch_config_with_callback]
    }
}