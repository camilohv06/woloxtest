package ar.com.wolox.androidtechnicalinterview.utils

class Constants {
    companion object {
        //Database
        const val TABLE_NAME = "favorite"
        const val DATABASE_NAME = "favorite_database"
        const val COLUMN_ID = "id"
        const val COLUMN_TITLE = "title"
        const val COLUMN_URL_ORIGINAL = "urlOriginal"
        const val COLUMN_URL_DOWNSIZED = "urlDownsized"

        //Firebase
        const val LIMIT_SEARCH = "LIMIT_SEARCH"
    }
}