package ar.com.wolox.androidtechnicalinterview.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ar.com.wolox.androidtechnicalinterview.utils.Constants
import org.jetbrains.annotations.NotNull

@Entity(tableName = Constants.TABLE_NAME)
data class Favorite(
    @PrimaryKey @ColumnInfo(name = Constants.COLUMN_ID) @NotNull var id: String,
    @ColumnInfo(name = Constants.COLUMN_TITLE) @NotNull var title: String,
    @ColumnInfo(name = Constants.COLUMN_URL_ORIGINAL) @NotNull val urlOriginal: String,
    @ColumnInfo(name = Constants.COLUMN_URL_DOWNSIZED) @NotNull val urlDownsized: String
)