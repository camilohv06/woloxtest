package ar.com.wolox.androidtechnicalinterview.database

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import ar.com.wolox.androidtechnicalinterview.models.Favorite

class FavoritesViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = FavoriteRepository(application)
    val contacts = repository.getAllFavoriteGif()

    fun saveFavoriteGif(favorite: Favorite) {
        repository.insert(favorite)
    }

    fun deleteFavoriteGif(id: String) {
        repository.deleteFavoriteGif(id)
    }
}