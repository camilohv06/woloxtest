package ar.com.wolox.androidtechnicalinterview.models

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ar.com.wolox.androidtechnicalinterview.utils.Constants

@Dao
interface FavoriteDao {
    @Query("SELECT * FROM " + Constants.TABLE_NAME)
    fun getAllFavoriteGif(): LiveData<List<Favorite>>

    @Query("SELECT * FROM " + Constants.TABLE_NAME + " WHERE " + Constants.COLUMN_ID + " = :id LIMIT 1")
    fun getAllFavoriteGiff(id: String): LiveData<List<Favorite>>

    @Query("SELECT * FROM " + Constants.TABLE_NAME + " WHERE " + Constants.COLUMN_ID + " = :id LIMIT 1")
    fun getFavoriteGif(id: String): Favorite

    @Insert
    fun insert(favorite: Favorite)

    @Update
    fun update(vararg favorite: Favorite)

    @Delete
    fun delete(vararg favorite: Favorite)

    @Query("DELETE FROM " + Constants.TABLE_NAME + " WHERE " + Constants.COLUMN_ID + " = :id")
    fun deleteFavoriteGif(id: String)
}