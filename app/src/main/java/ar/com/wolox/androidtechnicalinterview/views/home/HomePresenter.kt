package ar.com.wolox.androidtechnicalinterview.views.home

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.GridView
import android.widget.SearchView
import ar.com.wolox.androidtechnicalinterview.BuildConfig
import ar.com.wolox.androidtechnicalinterview.MainApplication
import ar.com.wolox.androidtechnicalinterview.R
import ar.com.wolox.androidtechnicalinterview.data.ApiDataManager
import ar.com.wolox.androidtechnicalinterview.data.network.RestApi
import ar.com.wolox.androidtechnicalinterview.error.*
import ar.com.wolox.androidtechnicalinterview.models.*
import ar.com.wolox.androidtechnicalinterview.utils.Constants.Companion.LIMIT_SEARCH
import ar.com.wolox.androidtechnicalinterview.utils.animateView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.File
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class HomePresenter(private var homeView: HomeView?) {

    private val compositeDisposable = CompositeDisposable()

    fun onResume() {}

    fun onDestroy() {
        homeView = null
    }

    fun fetchRemoteConfig(activity: Activity) {
        val config = MainApplication.getRemoteConfig()

        config.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d(activity.toString(), "Config params updated: $updated")
                }
            }
        MainApplication.setRemoteConfig(config)
    }

    fun loadGifTrending(lang: String) {
        if (hasStoragePermission(MainApplication.applicationContext())) {
            homeView?.showProgress()
            getGifTrending(lang)
        } else {
            homeView?.requestPermissionsWhenStart()
        }
    }

    fun onItemClicked(gif: Gif) {
        homeView?.showProgress()
        homeView?.downloadImageToShare(gif.images.fixed_height_downsampled.url)
    }

    fun onItemLongClicked(gif: Gif, selected: Boolean) {
        if (selected) {
            val favorite = Favorite(
                gif.id,
                gif.title,
                gif.images.original.url,
                gif.images.fixed_height_downsampled.url
            )
            homeView?.saveFavoriteGif(favorite)
        } else {
            homeView?.deleteFavoriteGif(gif.id)
        }
    }

    fun convertToGifDTO(favorites: List<Favorite>): List<Gif> {
        val gifList = ArrayList<Gif>()
        for (favorite in favorites) {
            val gifTemp = Gif(
                id = favorite.id, title = favorite.title, enable = true,
                images = Images(
                    ImageDetails(favorite.urlOriginal), ImageDetails(favorite.urlDownsized)
                )
            )
            gifList.add(gifTemp)
        }
        return gifList
    }

    fun setFavoriteStatus(items: List<Gif>, gifListFavorites: List<Gif>): List<Gif> {
        for (item in items) {
            for (favorite in gifListFavorites) {
                if (favorite.enable && item.id == favorite.id) {
                    item.enable = true
                }
            }
        }
        return items
    }

    fun changeTitleMenu(item: MenuItem, status: Boolean) {
        val idTitle: Int = if (status) R.string.txt_menu_all else R.string.txt_menu_favorite
        homeView?.setTitleMenu(item, idTitle)
    }

    fun setUIGridGif(gridGif: GridView?) {
        gridGif?.horizontalSpacing = 15 //TODO Pending set columns from firebase
        gridGif?.verticalSpacing = 15
        gridGif?.stretchMode = GridView.STRETCH_COLUMN_WIDTH
    }

    fun validateQuery(query: String?, lang: String) {
        if (!isValidQuery(query)) return
        if (!hasValidLength(query!!)) return

        getGifList(query, lang)
    }

    fun changeVisibilitySearchView(searchViewGif: SearchView?, menuStatus: Boolean) {
        if (menuStatus) {
            searchViewGif?.visibility = View.VISIBLE
            animateView(searchViewGif, R.anim.slide_down)
        } else {
            searchViewGif?.visibility = View.GONE
        }
    }

    fun hasStoragePermission(context: Context): Boolean {
        val permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permission != PackageManager.PERMISSION_GRANTED) return false
        return true
    }

    private fun getGifList(search_query: String, lang: String) {
        homeView?.showProgress()
        val apiDataManager = ApiDataManager(RestApi())

        compositeDisposable.add(
            apiDataManager.requestGifBySearchQuery(search_query, lang)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ data ->
                    validateSuccess(data)
                }, { throwable ->
                    validateError(throwable)
                })
        )
    }

    fun getGifTrending(lang: String) {
        homeView?.showProgress()
        val apiDataManager = ApiDataManager(RestApi())

        compositeDisposable.add(
            apiDataManager.requestGifTrending(lang)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ data ->
                    validateSuccess(data)
                }, { throwable ->
                    validateError(throwable)
                })
        )
    }

    private fun validateSuccess(gifResponse: GifResponse) {
        if (gifResponse.data.isEmpty()) {
            ErrorNoGif(homeView).showError()
        }

        homeView?.apply {
            setItems(gifResponse.data)
            updateGifList(gifResponse.data)
            hideProgress()
        }
    }

    private fun validateError(throwable: Throwable) {
        when (throwable) {
            is UnknownHostException -> ErrorUnknownHost(homeView).showError()
            is SocketTimeoutException -> ErrorSocketTimeout(homeView).showError()
            is HttpException -> ErrorSocketTimeout(homeView).showError()
            else -> ErrorGeneral(homeView).showError()
        }
    }

    private fun isValidQuery(query: String?): Boolean {
        if (query == null) {
            ErrorNoQuery(homeView).showError()
            return false
        }
        return true
    }

    private fun hasValidLength(query: String): Boolean {

        if (query.length < MainApplication.getRemoteConfig().getLong(LIMIT_SEARCH)) {
            ErrorSearchQuery(homeView).showError()
            return false
        }
        return true
    }

    fun shareWitSocialNetworks(activity: Activity, file: File) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        shareIntent.putExtra(Intent.EXTRA_STREAM, getUri(activity, file))
        activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.app_name)))
    }

    private fun getUri(activity: Activity, file: File): Uri {
        return FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".fileprovider", file)
    }
}