package ar.com.wolox.androidtechnicalinterview.error

import ar.com.wolox.androidtechnicalinterview.MainApplication
import ar.com.wolox.androidtechnicalinterview.R
import ar.com.wolox.androidtechnicalinterview.utils.showMessage
import ar.com.wolox.androidtechnicalinterview.views.home.HomeView

class ErrorUnknownHost(private val homeView: HomeView?) : ErrorInterface {

    override fun showError() {
        homeView?.hideProgress()
        showMessage(MainApplication.applicationContext().resources.getString(R.string.txt_error_no_internet))
    }
}