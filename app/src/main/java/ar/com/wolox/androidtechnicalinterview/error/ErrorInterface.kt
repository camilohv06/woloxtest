package ar.com.wolox.androidtechnicalinterview.error

interface ErrorInterface {
    fun showError()
}