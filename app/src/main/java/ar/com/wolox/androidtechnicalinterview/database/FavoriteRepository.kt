package ar.com.wolox.androidtechnicalinterview.database

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.os.AsyncTask
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.FavoriteDao

class FavoriteRepository(application: Application) {
    private val favoriteDao: FavoriteDao? = FavoriteDatabase.getInstance(application)?.favoriteDao()

    fun getAllFavoriteGif(): LiveData<List<Favorite>> {
        return favoriteDao?.getAllFavoriteGif() ?: MutableLiveData<List<Favorite>>()
    }

    @Throws(SQLiteConstraintException::class)
    fun insert(favorite: Favorite) {
        try {
            if (favoriteDao != null) InsertAsyncTask(favoriteDao).execute(favorite)
        } catch (e: SQLiteConstraintException) {
            println("Primary Key Violate")
        }

    }

    private class InsertAsyncTask(private val favoriteDao: FavoriteDao) :
        AsyncTask<Favorite, Void, Void>() {
        override fun doInBackground(vararg favorites: Favorite?): Void? {
            for (favorite in favorites) {
                if (favorite != null) {
                    try {
                        favoriteDao.insert(favorite)
                    } catch (e: SQLiteConstraintException) {
                        println("Primary Key Violate")
                    }
                }
            }
            return null
        }
    }

    fun deleteFavoriteGif(id: String) {
        if (favoriteDao != null) DeleteFavoriteGifAsyncTask(favoriteDao).execute(id)
    }

    private class DeleteFavoriteGifAsyncTask(private val favoriteDao: FavoriteDao) :
        AsyncTask<String, Void, Void>() {
        override fun doInBackground(vararg ids: String?): Void? {
            for (id in ids) {
                if (id != null) favoriteDao.deleteFavoriteGif(id)
            }
            return null
        }

    }
}