package ar.com.wolox.androidtechnicalinterview.views.home

import android.content.Intent
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.Gif
import ar.com.wolox.androidtechnicalinterview.models.ImageDetails
import ar.com.wolox.androidtechnicalinterview.models.Images
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.atLeast
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeGifListPresenterTest {

    @Mock
    lateinit var view: HomeView

    lateinit var presenter: HomePresenter
    lateinit var gif: Gif

    @Before
    fun setUp() {
        presenter = HomePresenter(view)
        gif = Gif("abc", "title", true, Images(ImageDetails("urlOriginal"), ImageDetails("urlDownsized")))

    }

    @Ignore
    @Test
    fun verifyWhenClickAboutGifAndActivateShareInSocialNetworksTest() {
        presenter.onItemClicked(gif)
        verify(view, atLeast(1)).openShareActivity(shareIntent = Intent())
    }

    @Test
    fun verifyWhenLongClickAndSaveInFavoritesTest() {
        presenter.onItemLongClicked(gif, true)
        verify(view).saveFavoriteGif(Favorite(gif.id, gif.title, gif.images.original.url, gif.images.downsized.url))
    }

}