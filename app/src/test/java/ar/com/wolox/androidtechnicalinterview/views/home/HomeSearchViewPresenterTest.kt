package ar.com.wolox.androidtechnicalinterview.views.home

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeSearchViewPresenterTest {

    @Mock
    lateinit var view: HomeView

    lateinit var presenter: HomePresenter

    @Before
    fun setUp() {
        presenter = HomePresenter(view)
    }

    @Test
    fun verifyErrorWhenNoExistSearchCriteriaTest() {
        presenter.validateQuery(null, "es")
        verify(view).showMessageErrorNoQuery()
    }

    @Test
    fun verifyErrorWhenThereAreLessCharacterAllowTest() {
        presenter.validateQuery("abc", "es")
        verify(view).showMessageErrorSearchQuery()
    }

}