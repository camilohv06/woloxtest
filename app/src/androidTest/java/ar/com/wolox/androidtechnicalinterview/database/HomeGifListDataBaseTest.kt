package ar.com.wolox.androidtechnicalinterview.database

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import ar.com.wolox.androidtechnicalinterview.models.Favorite
import ar.com.wolox.androidtechnicalinterview.models.FavoriteDao
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class HomeGifListDataBaseTest {

    private var favoriteDao: FavoriteDao? = null
    private var favoriteDatabase: FavoriteDatabase? = null

    @Before
    fun setup() {
        FavoriteDatabase.TEST_MODE = true
        favoriteDatabase = FavoriteDatabase.getInstance(InstrumentationRegistry.getTargetContext())
        favoriteDao = favoriteDatabase?.favoriteDao()
    }

    @After
    fun tearDown() {
        favoriteDatabase?.close()
    }

    @Test
    fun insertFavoriteGifTest() {
        val favorite = Favorite("abc123", "UnitTest", "original.url", "downsized.url")
        favoriteDao?.insert(favorite)

        val favoriteTest = getValue(favoriteDao?.getAllFavoriteGiff(favorite.id)!!)
        Assert.assertEquals(favorite.title, favoriteTest.title)
    }

    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: LiveData<T>): Favorite {
        val data = arrayOfNulls<Any>(1)
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(t: T?) {
                data[0] = t
                latch.countDown()
                liveData.removeObserver(this)//To change body of created functions use File | Settings | File Templates.
            }
        }
        liveData.observeForever(observer)
        latch.await(2, TimeUnit.SECONDS)

        val arrayList = data[0] as ArrayList<*>
        return arrayList[0] as Favorite
    }
}