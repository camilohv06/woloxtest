# WoloxTest
This is an application to show Gif's of [Giphy](https://giphy.com), also it's possible save Gif's Favorites and share Gif's with your friends.

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://bitbucket.org/camilohv06/woloxtest.git
```

## Configuration
### Dependencies and Libraries:

* Firebase: To remote config, testing and tracking bug.
* rxAndroid, rxJava, retrofit, gson: To communication with Api's and process of the answer.
* Glide: To Manage of Images and Gif's
* Room: To implement SQLite

## Maintainers
This project is mantained by:
* Camilo Henao [camilohenaov6@gmail.com](camilohenaov6@gmail.com)